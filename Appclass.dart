class AppOftheYear {
  var appName;
  var appCategory;
  var appDeveloper;
  var appYear;

  ShowAppInfo() {
    print('App Name is: ${appName}');
    print('App Category is: ${appCategory}');
    print('App Developer is: ${appDeveloper}');
    print('App year is: ${appYear}');
    print(appName.toUpperCase());
  }
}

void main() {
  var app = new AppOftheYear();

  app.appName = 'EasyEquities';
  app.appCategory = 'Best Consumer Solution';
  app.appDeveloper = 'Unknown';
  app.appYear = 2020;
  app.ShowAppInfo();
}
