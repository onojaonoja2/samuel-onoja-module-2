void main() {
  var apps = {
    2012: 'FNB',
    2013: 'SnapScan',
    2014: 'Live inspect',
    2015: 'Wumdrop',
    2016: 'Domestfly',
    2017: 'Shyft',
    2018: 'Kula ecosystem',
    2019: 'Naked insurance',
    2020: 'Easy Equities',
    2021: 'Edtech'
  };

  print(apps);
  print(apps[2017]);
  print(apps[2018]);
  print(apps.length);
}
